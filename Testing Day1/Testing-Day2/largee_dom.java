package Selectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;
import java.util.List;

public class largee_dom {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\vkeshavasa001\\Downloads\\chromedriver_win32\\chromedriver.exe");
		
		 WebDriver driver = new ChromeDriver();
	     driver.get("https://the-internet.herokuapp.com/");
	     driver.manage().window().maximize();
	     
	     driver.findElement(By.linkText("Large & Deep DOM")).click();
	     
	     List <WebElement> a= (List<WebElement>) driver.findElement(By.cssSelector("#sibling-12\\.2"));
	     System.out.println(a.get(0).getText());
	     driver.findElement(By.cssSelector("#sibling-12\\.3")).getText();
	     
	     driver.findElement(By.cssSelector("#sibling-13\\.2")).getText();
	     driver.findElement(By.cssSelector("#sibling-13\\.3")).getText();
	}

}
