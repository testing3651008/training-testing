package Selectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class challenging_dom {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 System.setProperty("webdriver.chrome.driver", "C:\\Users\\vkeshavasa001\\Downloads\\chromedriver_win32\\chromedriver.exe");
			
		 WebDriver driver = new ChromeDriver();
	     driver.get("https://the-internet.herokuapp.com/");
	     driver.manage().window().maximize();
	     
	     driver.findElement(By.linkText("Challenging DOM")).click();
	     driver.findElement(By.cssSelector("#content > div > div > div > div.large-10.columns > table > tbody > tr:nth-child(3)")).getText();
	     

	}

}
